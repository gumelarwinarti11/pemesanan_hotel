import 'package:flutter/material.dart';

class AboutPage extends StatefulWidget {
  AboutPage({Key key}) : super(key: key);

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('About')),
        backgroundColor: Colors.grey,
        body: SafeArea(
            child: Column(children: [
          Container(
              alignment: Alignment.center,
              child: Text(
                ' Dwi candra gumelar_18282005, CopyRight',
                style: TextStyle(fontSize: 22),
              ),
              color: Colors.blue[100],
              height: 664,
              padding: EdgeInsets.all(15)),
        ])));
  }
}
