import 'package:flutter/material.dart';
import 'package:pemesanan_hotel/about.dart';
import 'package:pemesanan_hotel/pribadi.dart';
import 'package:pemesanan_hotel/reservasi.dart';
import 'package:pemesanan_hotel/beranda.dart';
import 'package:pemesanan_hotel/pesan.dart';
import 'package:pemesanan_hotel/about.dart';

class BelajarNavBar extends StatefulWidget {
  @override
  _BelajarNavBarState createState() => _BelajarNavBarState();
}

class _BelajarNavBarState extends State<BelajarNavBar> {
  static String tag = 'home-page';
  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [
    new BerandaPage(),
    new ReservasiPage(),
    new PesanPage(),
    new AboutPage(),
    new Pribadi(),
  ];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _container[_bottomNavCurrentIndex],
        bottomNavigationBar: _buildBottomNavigation());
  }

  Widget _buildBottomNavigation() {
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (index) {
        setState(() {
          _bottomNavCurrentIndex = index;
        });
      },
      currentIndex: _bottomNavCurrentIndex,
      items: [
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home_work,
            color: Colors.blue,
          ),
          icon: new Icon(
            Icons.home_work,
            color: Colors.grey,
          ),
          title: new Text(
            'Home',
          ),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.post_add,
            color: Colors.blue,
          ),
          icon: new Icon(
            Icons.post_add,
            color: Colors.grey,
          ),
          title: new Text('Reservasi'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.messenger,
            color: Colors.blue,
          ),
          icon: new Icon(
            Icons.messenger,
            color: Colors.grey,
          ),
          title: new Text('pesan'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.person,
            color: Colors.blue,
          ),
          icon: new Icon(
            Icons.person,
            color: Colors.grey,
          ),
          title: new Text('About'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home_work,
            color: Colors.blue,
          ),
          icon: new Icon(
            Icons.account_box,
            color: Colors.grey,
          ),
          title: new Text(
            'Pribadi',
          ),
        ),
      ],
    );
  }
}
