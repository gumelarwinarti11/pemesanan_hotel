import 'package:flutter/material.dart';
import 'package:pemesanan_hotel/userViewModel.dart';

class PesanPage extends StatefulWidget {
  PesanPage({Key key}) : super(key: key);

  @override
  _PesanPageState createState() => _PesanPageState();
}

class _PesanPageState extends State<PesanPage> {
  List dataUser = new List();

  void getDataUser() {
    UserViewModel().getUsers().then((value) {
      setState(() {
        dataUser = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getDataUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Icon(Icons.hotel_outlined),
        ),
        body: Container(
            child: dataUser == null
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    itemCount: dataUser.length,
                    itemBuilder: (context, i) {
                      return Container(
                        margin: EdgeInsets.all(10.0),
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: Colors.blue[600]),
                        child: Column(
                          children: [
                            Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 50, vertical: 5.0),
                                color: Colors.blue[200],
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'ID Pemesanan :',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                    Text(
                                      dataUser[i]['id'],
                                      style: TextStyle(fontSize: 18),
                                    )
                                  ],
                                )),
                            Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 50, vertical: 10),
                                color: Colors.blue[200],
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Nama Pemesan :',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                    Text(
                                      dataUser[i]['nama_pemesan'],
                                      style: TextStyle(fontSize: 18),
                                    )
                                  ],
                                )),
                            Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 50, vertical: 10),
                                color: Colors.blue[200],
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Jenis Kamar:',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                    Text(
                                      dataUser[i]['jenis_kamar'],
                                      style: TextStyle(fontSize: 18),
                                    )
                                  ],
                                )),
                            Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 50, vertical: 10),
                                color: Colors.blue[200],
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Mulai Menginap :',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                    Text(
                                      dataUser[i]['mulai_menginap'],
                                      style: TextStyle(fontSize: 18),
                                    )
                                  ],
                                )),
                            Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 50, vertical: 10),
                                color: Colors.blue[200],
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Lama Menginap :',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                    Text(
                                      dataUser[i]['lama_menginap'],
                                      style: TextStyle(fontSize: 18),
                                    )
                                  ],
                                )),
                          ],
                        ),
                      );
                    },
                  )));
  }
}
