import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:pemesanan_hotel/pesan.dart';
import 'package:pemesanan_hotel/userPostModel.dart';
import 'package:pemesanan_hotel/userViewModel.dart';

class ReservasiPage extends StatefulWidget {
  @override
  ReservasiPageState createState() => ReservasiPageState();
}

//class controller
class ReservasiPageState extends State<ReservasiPage> {
  // EntryFormState(this.contact);

  TextEditingController jenisKamarController = TextEditingController();
  TextEditingController lamaMenginapController = TextEditingController();
  TextEditingController namaPemesanController = TextEditingController();
  TextEditingController mulaiMenginapController = TextEditingController();

  void addData() {
    UserpostModel commRequest = UserpostModel();
    commRequest.jenis_kamar = jenisKamarController.text;
    commRequest.lama_menginap = lamaMenginapController.text;
    commRequest.nama_pemesan = namaPemesanController.text;
    commRequest.mulai_menginap = mulaiMenginapController.text;

    UserViewModel()
        .postUser(userpostModelToJson(commRequest))
        .then((value) => print('success'));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: Icon(Icons.hotel_outlined),
            ),
            body: Padding(
              padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
              child: ListView(
                children: <Widget>[
                  // username
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: jenisKamarController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Jenis kamar',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),
                  // nama
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: lamaMenginapController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Lama menginap',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),

                  // deskripsi
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: namaPemesanController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Nama Pemesan',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),

                  // foto
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: mulaiMenginapController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Mulai Menginap',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),
                  new InkWell(
                    onTap: () {
                      addData();
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => PesanPage()));
                    },
                    child: Container(
                      width: 200,
                      padding: EdgeInsets.symmetric(vertical: 15),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey.shade200,
                                offset: Offset(2, 4),
                                blurRadius: 5,
                                spreadRadius: 2)
                          ],
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [Colors.blue[300], Colors.blueGrey])),
                      child: Text(
                        'Pesan sekarang',
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                  )
                ],
              ),
            )));
  }
}
