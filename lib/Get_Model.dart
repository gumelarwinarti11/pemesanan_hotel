import 'package:http/http.dart' as http;
import 'dart:convert';

class UserGet {
  String id;
  String firstName;
  String avatar;
  String lastName;
  String email;

  UserGet({this.id, this.firstName, this.avatar, this.lastName, this.email});

  factory UserGet.creatUserGet(Map<String, dynamic> object) {
    //return object PostResult yang baru
    return UserGet(
        id: object['id'].toString(),
        firstName: object['first_name'],
        avatar: object['avatar'],
        lastName: object['last_name'],
        email: object['email']);
  }

  //metode untuk menghubungkan ke API
  static Future<UserGet> connectTopApiUser(String id) async {
    String apiURLPOST = 'https://reqres.in/api/users/' + id;
    //http ini methode async lihat (future)
    //maka menggunakan await
    //calling api
    var apiResult = await http.get(apiURLPOST);

    //untuk mendapatkan bentuk json
    var jsonObject = json.decode(apiResult.body);

    var userData = (jsonObject as Map<String, dynamic>)['data'];

    //print(userData)

    // kembalikan nilainya
    return UserGet.creatUserGet(userData);
  }
}
