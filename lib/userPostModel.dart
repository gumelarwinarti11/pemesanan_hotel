import 'dart:convert';

List userModelFromJson(String str) => List.from(json.decode(str));

String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());

class UserpostModel {
  UserpostModel({
    this.jenis_kamar,
    this.lama_menginap,
    this.nama_pemesan,
    this.mulai_menginap,
  });

  String jenis_kamar;
  String lama_menginap;
  String nama_pemesan;
  String mulai_menginap;

  Map toJson() => {
        "JenisKamar": jenis_kamar,
        "LamaMenginap": lama_menginap,
        "NamaPemesan": nama_pemesan,
        "MulaiMenginap": mulai_menginap,
      };
}
