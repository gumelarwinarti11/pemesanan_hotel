import 'package:flutter/material.dart';
import 'package:pemesanan_hotel/beranda.dart';
import 'package:pemesanan_hotel/navbar.dart';
import 'package:pemesanan_hotel/registrasi.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Profil')),
        backgroundColor: Colors.white54,
        body: SafeArea(
            child: Column(children: [
          Container(
            color: Colors.white60,
            height: 450,
            padding: EdgeInsets.all(15),
            child: Column(
              children: [
                Text(
                  "Daftar dan nikmati promo nikmati berbagai promo khusus member, sekarang!!",
                  style: TextStyle(
                      fontSize: 17,
                      color: Colors.black,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  height: 40.0,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Registrasi()));
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    padding: EdgeInsets.all(0.0),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [Color(0xff374ABE), Color(0xff64B6FF)],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          borderRadius: BorderRadius.circular(30.0)),
                      child: Container(
                        constraints:
                            BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                        alignment: Alignment.center,
                        child: Text(
                          "Register",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 10),
                  margin: EdgeInsets.only(top: 10),
                  child: Text('OR'),
                ),
                Container(
                    height: 150,
                    width: 12000,
                    child: ListView(
                      children: [
                        TextField(
                          decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 15),
                            hintText: 'Username',
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextField(
                          decoration: InputDecoration(
                            hintStyle: TextStyle(fontSize: 15),
                            hintText: 'Password',
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                      ],
                    )),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  height: 40.0,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BelajarNavBar()));
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                    padding: EdgeInsets.all(0.0),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [Color(0xff374ABE), Color(0xff64B6FF)],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          borderRadius: BorderRadius.circular(30.0)),
                      child: Container(
                        constraints:
                            BoxConstraints(maxWidth: 300.0, minHeight: 40.0),
                        alignment: Alignment.center,
                        child: Text(
                          "Login",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            color: Colors.white60,
            padding: EdgeInsets.all(15),
            child: Row(
              children: [
                Icon(Icons.help),
                SizedBox(width: 10),
                Text('Pusat Bantuan')
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            color: Colors.white60,
            padding: EdgeInsets.all(15),
            child: Row(
              children: [
                Icon(Icons.menu_book_sharp),
                SizedBox(width: 10),
                Text('Kabar berita')
              ],
            ),
          )
        ])));
  }
}
