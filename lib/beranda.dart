import 'package:flutter/material.dart';
import 'package:pemesanan_hotel/kamar_model.dart';

class BerandaPage extends StatefulWidget {
  BerandaPage({Key key}) : super(key: key);

  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[100],
      appBar: AppBar(
        title: Icon(
          Icons.hotel_outlined,
        ),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
            child: Column(
          children: [
            Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            "https://2.bp.blogspot.com/-D4kf2G8j4-Y/XI8McW0HjBI/AAAAAAAAAL0/JCutpV-KvHMJMV0xfA63rPf-UFHg6ocowCLcBGAs/s1600/Gambar%2BHotel%2BPaling%2BPelik%2BDan%2BMenarik%2BDi%2BDunia6.jpg"),
                        fit: BoxFit.cover)),
                // color: Colors.blue[300],
                height: 250,
                padding: EdgeInsets.only(top: 65),
                margin: EdgeInsets.only(top: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.green,
                          radius: 40,
                          child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 37,
                              child: Icon(
                                Icons.airplanemode_on,
                                size: 45,
                              )), //CircleAvatar
                        ),
                        Text(
                          'Pesawat',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ),

                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.green,
                          radius: 40,
                          child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 37,
                              child: Icon(
                                Icons.home_work_outlined,
                                size: 45,
                              )), //CircleAvatar
                        ),
                        Text(
                          'Hotel',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.green,
                          radius: 40,
                          child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 37,
                              child: Icon(
                                Icons.bus_alert,
                                size: 45,
                              )), //CircleAvatar
                        ),
                        Text(
                          'Travel',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.green,
                          radius: 40,
                          child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 37,
                              child: Icon(
                                Icons.train_outlined,
                                size: 45,
                              )), //CircleAvatar
                        ),
                        Text(
                          'Kereta',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ), //CircleAvatar
                  ],
                )),
            //Batas  akhir layer 1
            //    Container(
            //    child: Text('Promo Hari ini',
            //      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
            //alignment: Alignment.topLeft,
            //padding: EdgeInsets.only(left: 10),
            //margin: EdgeInsets.only(top: 20),
            //),

            Container(
              child: _buildKamarAja(),
              color: Colors.blue[300],
              height: 250,
              padding: EdgeInsets.only(top: 0),
              margin: EdgeInsets.only(top: 20),
            ),
            Container(
              child: _buildkolamAja(),
              color: Colors.blue[300],
              height: 250,
              padding: EdgeInsets.only(top: 0),
              margin: EdgeInsets.only(top: 20),
            ),
            Container(
              child: _buildmakanAja(),
              color: Colors.blue[300],
              height: 250,
              padding: EdgeInsets.only(top: 0),
              margin: EdgeInsets.only(top: 20),
            ),
          ],
        )),
      ),
    );
  }

  Widget _buildKamarAja() {
    return new Container(
      padding: EdgeInsets.fromLTRB(16.0, 16.0, 0.0, 16.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          new Text(
            "Promo Hari ini ",
            style: new TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white),
          ),
          new Padding(
            padding: EdgeInsets.only(top: 8.0),
          ),
          new Text(
            "Pilihan Terbaik",
            style: new TextStyle(
                fontFamily: "Avilar",
                fontWeight: FontWeight.bold,
                fontSize: 18,
                color: Colors.white),
          ),
          new SizedBox(
            height: 172.0,
            child: new ListView.builder(
              itemCount: _letsKamarAja.length,
              padding: EdgeInsets.only(top: 12.0),
              physics: new ClampingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return _rowKamarAja(_letsKamarAja[index]);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _rowKamarAja(Kamar kamar) {
    return new Container(
      margin: EdgeInsets.only(right: 16.0),
      child: new Column(
        children: <Widget>[
          new ClipRRect(
            borderRadius: new BorderRadius.circular(8.0),
            child: new Image.network(
              kamar.image,
              scale: 1.0,
              cacheHeight: 132,
              cacheWidth: 132,
            ),
          ),
          new Padding(
            padding: EdgeInsets.only(top: 8.0),
          ),
          new Text(kamar.title,
              style: TextStyle(color: Colors.white, fontSize: 17)),
        ],
      ),
    );
  }

  List<Kamar> _letsKamarAja = [];
  List<Kolam> _letskolamAja = [];
  List<Makan> _letsmakanAja = [];

  @override
  void initState() {
    super.initState();
// ini siasi unutk list lets service

    // inisiasi untuk lets food
    _letsKamarAja.add(new Kamar(
        title: "Sweet room",
        image:
            "https://www.wellnessspots.com/wp-content/uploads/2020/07/hotel-met-jacuzzi-op-kamer-inntel-utrecht.jpg"));
    _letsKamarAja.add(new Kamar(
        title: "Superior Room",
        image:
            "https://hotelbritannia.be/wp-content/uploads/Hotel-Britannia-03-2000x1333.jpg"));
    _letsKamarAja.add(new Kamar(
        title: "Junior Suite Room",
        image:
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2020/02/4.-Junior-Suite-Room-sumber-gambar-Pixabay.jpg"));
    _letsKamarAja.add(new Kamar(
        title: "Suite Room",
        image:
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2020/02/5.-Suite-Room-sumber-gambar-Pixabay.jpg"));
    _letsKamarAja.add(new Kamar(
        title: "Presidential Suite",
        image:
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2020/02/6.-Presidential-room-sumber-gambar-6sqft.jpg"));

// inisiasi buat kolamaja
    _letskolamAja.add(new Kolam(
        title: "Sweet pool",
        image:
            "https://2.bp.blogspot.com/-1wtzuHuBakg/UWR5nO6c2AI/AAAAAAAAAK0/NAiA8POiJVQ/s1600/kolam+renang+termegah+2.jpg"));
    _letskolamAja.add(new Kolam(
        title: "Area joging",
        image:
            "https://tempat.com/blog/wp-content/uploads/2020/03/Taman-Ramah-Lingkungan-Kebayoran-Park-1.jpg"));
    _letskolamAja.add(new Kolam(
        title: "Taman",
        image:
            "https://upload.wikimedia.org/wikipedia/commons/3/35/Chahar_Bagh%2C_Iran%2C_Street_at_night.jpg"));
    _letskolamAja.add(new Kolam(
        title: "Restoran",
        image:
            "https://www.oteliletisim.com/resimler/altin-es-otel/buyuk/altin-es-otel-9.jpg"));
    _letskolamAja.add(new Kolam(
        title: "Gym",
        image:
            "https://www.spaziofitnessmirandola.it/wp-content/uploads/2020/02/9T2A0918.jpg"));

    // beda lagi boy

    _letsmakanAja.add(new Makan(
        title: "Nasi goreng",
        image:
            "https://www.goodnewsfromindonesia.id/wp-content/uploads/images/source/fachrezy/Resep-Dan-cara-Membuat-Nasi-Goreng-Jawa-Asli-Enak.jpg"));
    _letsmakanAja.add(new Makan(
        title: "Soto",
        image:
            "https://www.theboxsceneproject.org/wp-content/uploads/2017/09/soto-lamongan-nestl-indofood-citarasa-indonesia.jpeg"));
    _letsmakanAja.add(new Makan(
        title: "Gudeg",
        image:
            "https://www.indoindians.com/wp-content/uploads/2016/04/gudeg.jpg"));
    _letsmakanAja.add(new Makan(
        title: "Sate",
        image:
            "https://www.indoindians.com/wp-content/uploads/2016/03/Sate-Ayam-1024x683.jpg"));
    _letsmakanAja.add(new Makan(
        title: "seafood",
        image: "https://www.kibrispdr.org/data/gambar-makanan-seafood-14.jpg"));
  }

  Widget _buildkolamAja() {
    return new Container(
      padding: EdgeInsets.fromLTRB(16.0, 16.0, 0.0, 16.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          new Text(
            "Fasilitas hotel",
            style: new TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white),
          ),
          new Padding(
            padding: EdgeInsets.only(top: 8.0),
          ),
          new Text(
            " ",
            style: new TextStyle(
                fontFamily: "Avilar",
                fontWeight: FontWeight.bold,
                fontSize: 18,
                color: Colors.white),
          ),
          new SizedBox(
            height: 172.0,
            child: new ListView.builder(
              itemCount: _letsKamarAja.length,
              padding: EdgeInsets.only(top: 12.0),
              physics: new ClampingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return _rowkolamAja(_letskolamAja[index]);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _rowkolamAja(Kolam kolam) {
    return new Container(
      margin: EdgeInsets.only(right: 16.0),
      child: new Column(
        children: <Widget>[
          new ClipRRect(
            borderRadius: new BorderRadius.circular(8.0),
            child: new Image.network(
              kolam.image,
              scale: 1.0,
              cacheHeight: 132,
              cacheWidth: 132,
            ),
          ),
          new Padding(
            padding: EdgeInsets.only(top: 8.0),
          ),
          new Text(kolam.title,
              style: TextStyle(color: Colors.white, fontSize: 17)),
        ],
      ),
    );
  }

//batasnya boy 1 container

  Widget _buildmakanAja() {
    return new Container(
      padding: EdgeInsets.fromLTRB(16.0, 16.0, 0.0, 16.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          new Text(
            "Makanan Favorit",
            style: new TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white),
          ),
          new Padding(
            padding: EdgeInsets.only(top: 8.0),
          ),
          new Text(
            " ",
            style: new TextStyle(
                fontFamily: "Avilar",
                fontWeight: FontWeight.bold,
                fontSize: 18,
                color: Colors.white),
          ),
          new SizedBox(
            height: 172.0,
            child: new ListView.builder(
              itemCount: _letsKamarAja.length,
              padding: EdgeInsets.only(top: 12.0),
              physics: new ClampingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return _rowrestoranAja(_letsmakanAja[index]);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _rowrestoranAja(Makan makan) {
    return new Container(
      margin: EdgeInsets.only(right: 16.0),
      child: new Column(
        children: <Widget>[
          new ClipRRect(
            borderRadius: new BorderRadius.circular(8.0),
            child: new Image.network(
              makan.image,
              scale: 1.0,
              cacheHeight: 132,
              cacheWidth: 132,
            ),
          ),
          new Padding(
            padding: EdgeInsets.only(top: 8.0),
          ),
          new Text(makan.title,
              style: TextStyle(color: Colors.white, fontSize: 17)),
        ],
      ),
    );
  }
}
