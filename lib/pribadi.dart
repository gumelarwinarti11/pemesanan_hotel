import 'package:flutter/material.dart';
import 'Get_Model.dart';

class Pribadi extends StatefulWidget {
  @override
  _PribadiState createState() => _PribadiState();
}

class _PribadiState extends State<Pribadi> {
  UserGet userGet;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserGet.connectTopApiUser('5').then((value) {
      setState(() {
        userGet = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: Icon(
              Icons.hotel_outlined,
            ),
          ),
          body: _bodyProfile()),
    );
  }

  Widget _bodyProfile() {
    return Center(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: Colors.blue[100],
        alignment: Alignment.center,
        padding: EdgeInsets.all(30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              "WELCOME!",
              style: TextStyle(fontSize: 48, color: Color(0xff0563BA)),
            ),
            Container(
                child: Column(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(70.0),
                  child: Image.network(
                    (userGet != null)
                        ? userGet.avatar
                        : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3YaKRGHiI0Jgg7Xp35rFtHQHS8GTPqGINEg&usqp=CAU",
                    cacheHeight: 128,
                    cacheWidth: 128,
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    (userGet != null)
                        ? userGet.firstName + " " + userGet.lastName
                        : "Data Gagal Diambil",
                    style: TextStyle(fontSize: 24, color: Color(0xff0563BA)),
                  ),
                ),
                Text(
                  (userGet != null) ? userGet.email : "Data gagal Diambil",
                  style: TextStyle(fontSize: 18, color: Colors.black),
                )
              ],
            ))
          ],
        ),
      ),
    );
  }
}
